#!/usr/bin/env bash

export BUILDAH_FORMAT=docker

set -o errexit

from_tag=docker.io/alpine:3.12

# Use some environment variables here so we can provide them in CI pipelines.
container=$(buildah from $from_tag)
registry_image=${REGISTRY_IMAGE:-registry.hub.docker.com/stemid/flask-boilerplate}
registry_tag=${REGISTRY_TAG:-master}

test -f "$REGISTRY_AUTH_JSON" && cp -v "$REGISTRY_AUTH_JSON" ./auth.json

buildah config --label maintainer="Stefan Midjich <swehack at gmail.com>" $container

buildah config --workingdir /app $container

buildah run $container apk -U upgrade
buildah run $container apk --update-cache add curl bash python3 py3-pip
buildah run $container mkdir /app/app

buildah copy $container ../requirements.txt
buildah copy $container ../app /app/app/
buildah copy $container ../run.py

buildah run $container pip install -r /app/requirements.txt

buildah config --entrypoint '["/usr/bin/python3", "run.py"]' $container

echo "$0: buildah commit"
buildah commit $container flask-boilerplate:$registry_tag

if [ -r ./auth.json ]; then
  echo "$0: buildah push"
  buildah push --authfile ./auth.json localhost/flask-boilerplate:$registry_tag docker://$registry_image:$registry_tag
  buildah push --authfile ./auth.json localhost/flask-boilerplate:$registry_tag docker://$registry_image:latest
fi
